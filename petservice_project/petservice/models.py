from django.db import models
from django.contrib.auth.models import User

from django.conf import settings

# Create your models here.


class Feedback(models.Model):
    services_text = models.CharField(max_length=200)
    posts_text = models.CharField(max_length=200,null=True)
    users =models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    pub_date = models.DateTimeField('date published')
    likes = models.IntegerField(default=0)

