from django.contrib import admin
from .models import Feedback

from .models import User
# Register your models here.


class FeedbackLookAdmin(admin.ModelAdmin):
    list_display = ['pk','users','services_text','posts_text','likes', 'pub_date']
admin.site.register(Feedback,FeedbackLookAdmin)