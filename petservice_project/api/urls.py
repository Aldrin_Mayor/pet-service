from django.urls import path

from . import views

urlpatterns = [
        path('', views.apiOverview, name="api-overview"),
        path('user-list/', views.userList, name="user-list" ),
        path('feedback-list/', views.feedbackList, name="feedback-list" ),
        path('feedback-create/', views.feedbackCreate, name='feedback-create'),
        path('feedback-detail/<str:petservice_id>/', views.feedbackDetail, name='feedback-detail'),
        path('feedback-api/<str:petservice_id>', views.feedbackApi, name='feedback-api'),     
        
]