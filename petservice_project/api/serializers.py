from django.db.models import fields
from rest_framework import serializers
from petservice.models import User
from petservice.models import Feedback
from django.forms import ModelForm



class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields='__all__'

class FeedbackSerializers(ModelForm):
    class Meta:
        model = Feedback
        fields=['users','services_text','posts_text']
