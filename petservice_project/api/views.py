from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .serializers import UserSerializers
from .serializers import FeedbackSerializers


from petservice.models import User
from petservice.models import Feedback


@api_view(['GET'])
def apiOverview(request):
	api_urls = {
		'user-list':'/user-list/',
		'feedback-list':'/feedback-list/',
		'feedback-detail':'/feedback-detail/<str:petservice_id>/',
        'feedback-api':'/feedback-api/<str:petservice_id>',
	}
	return Response(api_urls)

@api_view(['GET'])
def userList(request):
    petservice = User.objects.all()
    serializer = UserSerializers(petservice, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def feedbackList(request):
    petservice = Feedback.objects.all()
    serializer = FeedbackSerializers(petservice, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def feedbackCreate(request):
    serializer = FeedbackSerializers(data=request.data)

    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['GET'])
def feedbackDetail(request,petservice_id):
    petservice = Feedback.objects.get(id=petservice_id)
    serializer = FeedbackSerializers(petservice, many=False)
    return Response(serializer.data)

@api_view(['POST'])
def feedbackApi(request,petservice_id):
    petservice = Feedback.objects.get(id=petservice_id)
    serializer = FeedbackSerializers(instance=petservice, data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)