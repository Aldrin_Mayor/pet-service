from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, response
from django.contrib.auth.forms import UserCreationForm
from api.serializers import FeedbackSerializers
from django.contrib import messages
from datetime import datetime
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from petservice.models import Feedback


# Create your views here.

# index page
def index(request):
    return render(request, "petservice/index.html")

@login_required
#home page
def home(request):
    return render(request, "petservice/home.html")

#signup page
def signup(request):
    if request.method =="POST":
        form = UserCreationForm(request.POST)

        if form.is_valid():
            form.save()
            
            messages.success(request,"New Client Created! Please Sign in.")
            
            return redirect('index')
            
    else:
        form = UserCreationForm()

    return render(request, "registration/signup.html", {"form":form})


def home(request):
    latest_feedback_list = Feedback.objects.order_by('-pub_date')[:5]
    context = {
        'latest_feedback_list':latest_feedback_list,
    }
    return render(request, 'petservice/home.html', context)


# Getting Feedback Verions 1
def feedback(request, feedback_id):
    feedback = get_object_or_404(Feedback, pk=feedback_id)
    context = {
        'feedback':feedback,
    }
    return render(request, 'petservice/feedback.html' , context)

# Getting Feedback Version 2
def feedback_v2(request, feedback_id):
    feedback = get_object_or_404(Feedback, pk=feedback_id)
    return render(request, 'petservice/feedback.html' , {'feedback':feedback})

# Add Feedback
def feedbackForm(request):
    if request.method == 'POST':
        form = FeedbackSerializers(request.POST, request.FILES)
        if form.is_valid():
            new_feedback = form.save(commit=False) 
            new_feedback.user = request.user
            new_feedback.pub_date = datetime.now()
            new_feedback.save()
            
            messages.info(request, 'you have successfully added a feedback.')
            return redirect('feedback_form')
    else:
        form = FeedbackSerializers()
    context = {
        'form':form,
    }
    return render(request, 'petservice/addfeedback.html', context)

def feedbackLike(request, pk):
    pass