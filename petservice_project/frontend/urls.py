from django.urls import path

from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('', views.index, name="index"),
    path('home', views.home, name="home"),
    path('signup', views.signup, name="signup"),
    path('login', auth_views.LoginView.as_view(),name='login'),
    path('logout', auth_views.LogoutView.as_view(),name='logout'),
    path('feedback/<int:feedback_id>', views.feedback, name='feedback'),
    path('feedback_v2/<int:feedback_id>', views.feedback_v2, name='feedback_v2'),
    path('feedback_form', views.feedbackForm, name='feedback_form'),
    path('like/<int:pk>', views.feedbackLike, name='feedback_like'),

    
    
]